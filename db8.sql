use master
CREATE CERTIFICATE Cert3
    ENCRYPTION BY PASSWORD = N'aa'
    WITH SUBJECT = 'check',
    START_DATE = '08/12/2023'

CREATE MASTER KEY ENCRYPTION BY PASSWORD = 'P@ssw0rd';

use forum
insert into Tags (Name)
values(EncryptByCert(Cert_ID('Cert3'),'name'))
insert into Tags (Name)
values(EncryptByCert(Cert_ID('Cert1'),'name'))
insert into Tags (Name)
values(EncryptByCert(Cert_ID('Cert1'),'name'))

select convert(nvarchar(max), DecryptByCert(Cert_ID('Cert1'),Name, N'11') )
from Tags


CREATE ASYMMETRIC KEY ASymKey1
    WITH ALGORITHM = RSA_2048 
    ENCRYPTION BY PASSWORD = '11'

insert into Tags (Name)
values(ENCRYPTBYASYMKEY(ASYMKEY_ID('ASymKey1'), N'111001') )
insert into Tags (Name)
values(ENCRYPTBYASYMKEY(ASYMKEY_ID('ASymKey1'), N'111002') )
insert into Tags (Name)
values(ENCRYPTBYASYMKEY(ASYMKEY_ID('ASymKey1'), N'111003') )



CREATE SYMMETRIC KEY SymKey1
    WITH ALGORITHM = Aes_256 
    ENCRYPTION BY PASSWORD = '11'

OPEN SYMMETRIC KEY SymKey1 DECRYPTION BY
    PASSWORD = '11'

insert into Tags (Name)
values(EncryptByKey(Key_GUID('SymKey1'), convert(nvarchar(max),'111001') ))


insert into Tags (Name)
values(EncryptByPassPhrase('Password', N'111004'))

select
    convert(nvarchar(max), DecryptByPassPhrase('Password', Name))
from Tags


BACKUP MASTER KEY TO FILE = 'c:\backup\key.bak'
    ENCRYPTION BY PASSWORD = 'P@ssw0rd'

CREATE CERTIFICATE TDECertificate WITH SUBJECT ='DESKTOP-LVDBVRP\lyamo', start_date ='08/12/2023'


BACKUP CERTIFICATE TDECertificate
    TO FILE = 'c:\backup\bb'
    WITH PRIVATE KEY
        (
        FILE ='c:\backup\dd',
        ENCRYPTION BY PASSWORD = 'Password#3'
        );


-- GRANT VIEW DEFINITION ON CERTIFICATE::TDECertificate TO [DESKTOP-LVDBVRP\lyamo]

CREATE DATABASE ENCRYPTION KEY
    WITH ALGORITHM = AES_256 
    ENCRYPTION BY SERVER CERTIFICATE TDECertificate;

ALTER DATABASE [Forum]
    SET ENCRYPTION ON ;


SELECT DB_NAME(database_id), * FROM sys.dm_database_encryption_keys


SELECT DB_NAME(database_id), encryption_state, percent_complete FROM
    sys.dm_database_encryption_keys

--------
USE master
go
-- Создаем главный ключ базы данных master
IF (not EXISTS(SELECT *
               FROM sys.symmetric_keys
               WHERE name =
                     '##MS_DatabaseMasterKey##'))
-- Создаем сертификат, которым будем шифровать DEK
CREATE CERTIFICATE DEK_EncCert WITH SUBJECT = 'DEK Encryption Certificate'
go
-- Создаем базу данных, которую будем шифровать
CREATE DATABASE MySecretDB
go
-- И сразу делаем ее полную резервную копию (секретных данных здесь нет)
BACKUP DATABASE MySecretDB TO DISK = 'c:\backup\MySecretDB.bak' WITH INIT
go

USE MySecretDB
go
-- Создаем таблицу и заполняем ее секретными данными
-- Делаем это в транзакции с меткой T1

BEGIN TRAN T1 WITH MARK
CREATE TABLE dbo.MySecretTable (Data varchar(200) not null)
INSERT dbo.MySecretTable (Data) VALUES ('It is my secret')
COMMIT
go
-- Шифруем базу данных
CREATE DATABASE ENCRYPTION KEY WITH ALGORITHM = AES_256
    ENCRYPTION BY SERVER CERTIFICATE DEK_EncCert
go
ALTER DATABASE MySecretDB SET ENCRYPTION ON
go

SELECT DB_NAME(database_id), encryption_state FROM
    sys.dm_database_encryption_keys

BACKUP LOG MySecretDB TO DISK = 'c:\backup\MySecretDB2.trn'


USE master
go
DROP DATABASE MySecretDB
go
DROP CERTIFICATE DEK_EncCert
go

RESTORE DATABASE MySecretDB FROM DISK = 'C:\backup\MySecretDB.bak' WITH
    NORECOVERY
RESTORE LOG MySecretDB FROM DISK = 'c:\backup\MySecretDB2.trn' WITH STOPATMARK
    = 'T1'
go
USE MySecretDB
go
-- Запрос к секретным данным
SELECT * FROM dbo.MySecretTable
go