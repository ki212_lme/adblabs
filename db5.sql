BACKUP DATABASE Forum
TO DISK = 'C:\SQLdata\BACKUPS\Forum_FullDbBkup.bak' WITH INIT,
NAME = 'Forum Full Db backup',
DESCRIPTION = 'Forum Full Database Backup'


RESTORE DATABASE Forum
    FROM DISK = 'C:\SQLdata\BACKUPS\Forum_FullDbBkup.bak'
WITH RECOVERY, REPLACE

BACKUP DATABASE Forum
    TO DISK = 'C:\SQLdata\BACKUPS\Forum_FullDbBkup.bak'
WITH INIT, NAME = 'Forum Full Db backup'

BACKUP LOG Forum
    TO DISK = 'C:\SQLdata\BACKUPS\Forum_FullDbBkup.bak'
WITH NOINIT, NAME = 'Forum Translog backup',
DESCRIPTION = 'AdventureWorks Transaction Log Backup', NOFORMAT