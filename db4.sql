use Forum

--     1. ДОСЛІДЖЕННЯ КУЧІ
-- Структуру SQL Server простіше зрозуміти за допомогою прикладів. Наступний код 
-- створює таблицю, організовану як кучі.
create table test_index
(
    id    int       not null,
    pole1 char(36)  not null,
    pole2 char(216) not null
)

select OBJECT_NAME ( object_id ) as table_name ,
       name as index_name , type , type_desc
from sys . indexes
where OBJECT_ID = OBJECT_ID ( N'test_index ' )

exec dbo.sp_spaceused @objname=N'test_index ', @updateusage=true

insert into test_index
values ( 1 , ' a' , 'b ' )

declare @i as int = 31
while @i < 240
    begin
        set @i = @i+ 1 ;
        insert into test_index
        values ( @i , ' a' , 'b ' )
    end ;


DECLARE @dbid INT = DB_ID(N'forum');
DECLARE @objectid INT = OBJECT_ID(N'test_index');
DECLARE @indexid INT = INDEXPROPERTY(@objectid, N'test_index', N'0');

-- Отримати фізичні статистичні дані
SELECT index_type_desc, page_count, record_count, avg_page_space_used_in_percent
FROM sys.dm_db_index_physical_stats(@dbid, @objectid, @indexid, NULL, 'DETAILED');


exec dbo.sp_spaceused @objname=N'test_index ', @updateusage=true


-- 2

truncate table test_index
create clustered index idx_cl_id on test_index ( id )


declare @i as int = 0
while @i < 18630
    begin
        set @i = @i + 1 ;
        insert into test_index
        values ( @i , ' a' , 'b ' )
    end ;

DECLARE @objectid INT = OBJECT_ID(N'test_index');
DECLARE @indexid INT = INDEXPROPERTY(@objectid, N'test_index', N'idx_ncl_pole1');
select index_type_desc , index_depth , index_level , page_count ,
record_count , avg_page_space_used_in_percent ,
avg_fragmentation_in_percent
from sys . dm_db_index_physical_stats
    ( db_id( N'forum' ), OBJECT_ID ( N'test_index' ), @indexid ,
    null , 'Detailed' )

     
truncate table test_index
declare @i as int = 0
while @i < 8906
    begin
        set @i = @i + 1 ;
        insert into test_index
        values ( @i % 100 , ' a' , 'b ' )
    end ;


insert into test_index
values ( 8909 % 100 , ' a' , 'b ' )


truncate table test_index
drop index idx_cl_id on test_index
create clustered index idx_cl_pole1 on test_index ( pole1 )
declare @i as int = 0
while @i < 9000
    begin
        set @i = @i + 1 ;
        insert into test_index
        values ( @i , format ( @i , '0000' ), 'b' )
    end ;

truncate table test_index
declare @i as int = 0
while @i < 9000
    begin
        set @i = @i + 1 ;
        insert into test_index
        values ( @i , cast ( newid () as char ( 36 )), 'b' )
    end ;

-- індекс на купі 
drop index idx_cl_pole1 on test_index
create nonclustered index idx_ncl_pole1 on test_index ( pole1 )
truncate table test_index
declare @i as int = 0

while @i < 24472
    begin
        set @i = @i + 1 ;
        insert into test_index
        values ( @i , format ( @i , '0000' ), 'b' )
    end ;
    
insert into test_index
values ( 24473 , ' 000024473' , 'b ' )

drop index idx_cl_id on test_index

create clustered index idx_cl_pid on test_index ( id )
create nonclustered index idx_ncl_pole1 on test_index ( pole1 )
truncate table test_index

declare @i as int = 0
while @i < 28864
    begin
        set @i = @i + 1 ;
        insert into test_index
        values ( @i , format ( @i , '0000' ), 'b' )
    end ;
insert into test_index
values ( 28865 , ' 000028865' , 'b ' )


SELECT
    sysobjects.name AS Таблиця,
    sysindexes.name AS Індекс,
    sysindexes.indid AS Номер
FROM
    sysobjects
        INNER JOIN sysindexes ON sysobjects.id = sysindexes.id
WHERE
        sysobjects.xtype = 'U'
  AND sysindexes.indid > 0
ORDER BY
    sysobjects.name,
    sysindexes.indid;



CREATE NONCLUSTERED INDEX IX_Answers_PostId
    ON dbo.Answers(PostId)
    WITH (FILLFACTOR = 70);

SELECT
    idx.name AS IndexName
FROM
    sys.indexes AS idx
        INNER JOIN
    sys.tables AS tbl ON idx.object_id = tbl.object_id
WHERE
        tbl.name = 'Company';


-- Створення унікального складеного індексу на таблиці dbo.Company
create nonclustered index uq
    ON dbo.Company(Name, Address);


CREATE INDEX IX_Company_Name_Address
    ON dbo.Company(Name, Address);

CREATE INDEX IX_Company_Address_Name
    ON dbo.Company(Address, Name);

drop index IX_Company_Address_Name on Company
drop index IX_Company_Name_Address on Company


SET STATISTICS TIME ON;

SELECT *
FROM dbo.Company
WHERE Name = 'Acme Inc.' ;

SET STATISTICS TIME OFF;


